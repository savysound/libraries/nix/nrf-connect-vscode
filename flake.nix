{
  description = "Build nRF Connect projects with Nix and VSCodium";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    nrf-connect-sdk.url = "git+https://gitlab.com/savysound/libraries/nix/nrf-connect-sdk";
    nrf-connect-sdk.inputs.nixpkgs.follows = "nixpkgs";
    nrf-connect-sdk.inputs.flake-utils.follows = "flake-utils";
    
    zephyr-sdk.url = "git+https://gitlab.com/savysound/libraries/nix/zephyr";
    zephyr-sdk.inputs.nixpkgs.follows = "nixpkgs";
    zephyr-sdk.inputs.flake-utils.follows = "flake-utils";
    
    nrfutil-nix.url = "git+https://gitlab.com/savysound/libraries/nix/nrfutil";
    nrfutil-nix.inputs.nixpkgs.follows = "nixpkgs";
    
    nrf-command-line-tools.url = "git+https://gitlab.com/savysound/libraries/nix/nrf-command-line-tools";
    nrf-command-line-tools.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { 
    self,
    nixpkgs,
    flake-utils,
    nrf-connect-sdk,
    zephyr-sdk,
    nrfutil-nix,
    nrf-command-line-tools
  }: flake-utils.lib.eachDefaultSystem (system: 
    let
      pkgs = import nixpkgs { inherit system; config.allowUnfree = true; };
      pythonEnv = nrf-connect-sdk.packages.${system}.pythonEnv;
      zephyr-sdk-arm = zephyr-sdk.devShells.${system}.arm;
      nrfutil = nrfutil-nix.packages.${system}.nrfutil;
      nrfclt = nrf-command-line-tools.packages.${system};
    in { 
      devShells.default = pkgs.mkShell {
        packages = [
          pkgs.cmake
          pkgs.gperf
          pkgs.ninja

          pythonEnv
          nrfutil
          nrfclt.nrf-command-line-tools
          nrfclt.segger-jlink-pack
        ];

        inputsFrom = [ zephyr-sdk-arm ];
      };
    });
}