# nRF Connect VSCode Development Flake

## Usage

To try out the environment:
```
nix develop
```

## Acknowledgments

[nrf-nix](https://github.com/MatthewCroughan/nrf-nix/tree/main)

